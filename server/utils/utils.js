exports.roundNumber = function(number,decimales = 2){
    return Math.round(number * Math.pow(10, decimales)) / Math.pow(10, decimales);
}

exports.getNumeroDias = function(firstDate,secondDate){
    var oneDay = 24*60*60*1000; 
    return Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
}

exports.validarEmail = function(email){
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var val = re.test(String(email).toLowerCase());
    return val;
}
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const mongoConnection = require('./config/mongoConfig');
const app = express();

global.router = express.Router();
global.routerViews = express.Router();

require('require-all')(__dirname + '/routes/api');
require('require-all')(__dirname + '/routes');

//configuracion
app.set("port",process.env.PORT || 80)
app.use(cors());

app.use(bodyParser.urlencoded({extended:false,limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));

//rutas
app.use('/api', router);
app.use('/', routerViews);

//archivos estaticos
app.use(express.static(path.join(__dirname,'public')));

mongoConnection.conectar(function(st,db){
    if(st){
        console.log("conectado a mongodb.");
        app.listen(app.get('port'), () => {
            console.log("server en puerto: ",app.get('port'));
        });
    }
});
'use strict'
var mongo = require('mongodb'); 
const collectionName = "Productos";
const util = require("../utils/utils");
const fs = require("fs");

exports.getProductosByCategoria = async function (idCategoria){
    try{
        var data = await db.collection(collectionName).find({
            IdCategoria: mongo.ObjectId(idCategoria),
            Visible: "SI"
        }).toArray();
        return data;
    }catch(err){
        return undefined;
    }
};

exports.getProductosByEmpresaAll = async function (idEmpresa){
    try{
        /*var data = await db.collection(collectionName).find({
            IdEmpresa: mongo.ObjectId(idEmpresa)
        }).toArray();*/
        var data = await db.collection(collectionName).aggregate([
            {
                $match:  { IdEmpresa: mongo.ObjectId(idEmpresa) },
            },
            {
                $lookup : { from: "Categorias", localField: "IdCategoria", foreignField: "_id", as: "Categoria" }
            },
            { $unwind: "$Categoria" } 
        ]).toArray();
        return data;
    }catch(err){
        return undefined;
    }
};

exports.getProductosByCodigoEmpresa = async function (codigo,idEmpresa){
    try{
        var data = await db.collection(collectionName).findOne({
            Codigo: codigo,
            IdEmpresa: mongo.ObjectId(idEmpresa)
        });
        return data;
    }catch(err){
        return undefined;
    }
};

exports.getProductosById = async function (id){
    try{
        var data = await db.collection(collectionName).findOne({
            _id: mongo.ObjectId(id)
        });
        return data;
    }catch(err){
        return undefined;
    }
};

exports.post = async function (data){
    var idProducto = mongo.ObjectId();
    var fileName = exports.saveFotoProducto(idProducto,data);
    var registro = {
        _id: idProducto,
        IdEmpresa: mongo.ObjectId(data.IdEmpresa),
        IdCategoria: mongo.ObjectId(data.IdCategoria),
        Codigo: data.Codigo,
        Nombre: data.Nombre,
        Descripcion: data.Descripcion,
        Precio: data.Precio,
        Visible: data.Visible,
        UrlImg: fileName,
        Estado: "ACTIVO"
    };
    await db.collection(collectionName).insertOne(registro);
    return registro._id;
}

exports.put = async function(id,data){
    var obj = {
        IdCategoria: mongo.ObjectId(data.IdCategoria),
        Codigo: data.Codigo,
        Nombre: data.Nombre,
        Descripcion: data.Descripcion,
        Precio: data.Precio,
        Visible: data.Visible
    };
    return await db.collection(collectionName).updateOne({_id: mongo.ObjectId(id)},{$set: obj});
}

exports.putFoto = async function(id,data){
    var fileName = exports.saveFotoProducto(id,data);
    var obj = {
        UrlImg: fileName
    };
    return await db.collection(collectionName).updateOne({_id: mongo.ObjectId(id)},{$set: obj});
}

exports.saveFotoProducto = function(idProducto,data){
    var fileName = null;
    if(data.Archivo != null && data.Archivo != undefined){
        if(data.Archivo.Data != ""){
            var path = "assets/images/products/";
            fileName = idProducto + "." + data.Archivo.Extension;
            fs.writeFile(path+fileName, data.Archivo.Data, 'base64', function(err) {});
        }
    }
    return fileName;
}
'use strict'
var md_auth = require('../middlewares/authenticated');
var mongo = require('mongodb'); 

const collectionName = "Usuarios";

const util = require("../utils/utils");

exports.getUsuariosAll = async function (){
    try{
        var data = await db.collection(collectionName).find({}).toArray();
        return data;
    }catch(err){
        return undefined;
    }
};

exports.post = async function (data){
    var registro = {
        _id: mongo.ObjectId(),
        IdEmpresa: mongo.ObjectId(data.IdEmpresa),
        Nombre: data.Nombre,
        Identificacion: data.Identificacion,
        Email: data.Email,
        Telefono: data.Telefono,
        Pass: data.Pass
    };
    await db.collection(collectionName).insertOne(registro);
    return registro._id;
}

exports.login = async function(data){
    var usuario = await db.collection(collectionName).findOne({Email: data.Email});
    if(usuario == undefined || usuario == null){
        return {
            Error: true,
            Msg: "Usuario no registrado en el sistema"
        };
    }

    if(usuario.Pass != data.Pass){
        return {
            Error: true,
            Msg: "Contraeña incorrecta"
        };
    }

    var objTokenUser = {
        Id: usuario._id,
        IdEmpresa: usuario.IdEmpresa,
        Nombre: usuario.Nombre,
        Email: usuario.Email
    };

    var dataRes = {
        Error: false,
        Msg : "OK",
        User: {
            Nombre: usuario.Nombre,
            Email: usuario.Email
        },
        Token:  md_auth.generateToken(objTokenUser)
    }

    return dataRes;
};

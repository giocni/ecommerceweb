'use strict'
var mongo = require('mongodb'); 
const collectionName = "Categorias";
const util = require("../utils/utils");

exports.getCategoriaById = async function (id){
    try{
        var data = await db.collection(collectionName).findOne({
            _id: mongo.ObjectId(id)
        });
        return data;
    }catch(err){
        return undefined;
    }
};

exports.getCategoriasAll = async function (){
    try{
        var data = await db.collection(collectionName).find({}).toArray();
        return data;
    }catch(err){
        return undefined;
    }
};
'use strict'

const mData = require("../data/productosData");
const mDataCategoria = require("../data/categoriasData");
const utils = require("../utils/utils");

exports.validarPost = async function (body){
    if(body.IdCategoria == undefined){
        return {Error: true, Msg: "Campo 'IdCategoria' no encontrado"};
    }
	if(body.Codigo == undefined){
        return {Error: true, Msg: "Campo 'Codigo' no encontrado"};
    }
    if(body.Nombre == undefined){
        return {Error: true, Msg: "Campo 'Nombre' no encontrado"};
    }
    if(body.Descripcion == undefined){
        return {Error: true, Msg: "Campo 'Descripcion' no encontrado"};
    }
    if(body.Precio == undefined){
        return {Error: true, Msg: "Campo 'Precio' no encontrado"};
    }
    if(body.Visible == undefined){
        return {Error: true, Msg: "Campo 'Visible' no encontrado"};
    }
    var categoria = await mDataCategoria.getCategoriaById(body.IdCategoria);
    if(categoria == null || categoria == undefined){
        return {Error: true, Msg: "La categoria seleccionada no es válida"};
    }
    if(body.Codigo.trim() == ""){
        return {Error: true, Msg: "El código ingresado no es válido"};
    }
    var prod = await mData.getProductosByCodigoEmpresa(body.Codigo.trim(), body.IdEmpresa);
    if(prod != null && prod != undefined){
        return {Error: true, Msg: "Ya existe un producto con este código"};
    }
    if(body.Nombre.trim() == ""){
        return {Error: true, Msg: "El nombre ingresado no es válido"};
    }
    if(body.Descripcion.trim() == ""){
        return {Error: true, Msg: "La descripción ingresada no es válida"};
    }
    try{
        body.Precio = parseFloat(body.Precio);
        if(body.Precio < 0 || isNaN(body.Precio)){
            return {Error: true, Msg: "El precio ingresado no es válido"};
        }
    }catch(err){
        return {Error: true, Msg: "El precio ingresado no es válido"};
    }
    if(body.Visible.trim() != "SI" && body.Visible.trim() != "NO"){
        return {Error: true, Msg: "El dato del campo visible no es válido"};
    }
    return {Error: false, Msg: "Guardado correctamente"};
};

exports.validarPut = async function (idProducto,body){
    var prod = await mData.getProductosById(idProducto);
    if(prod == null || prod == undefined){
        return {Error: true, Msg: "El producto no es válido"};
    }
    if(body.IdCategoria == undefined){
        return {Error: true, Msg: "Campo 'IdCategoria' no encontrado"};
    }
	if(body.Codigo == undefined){
        return {Error: true, Msg: "Campo 'Codigo' no encontrado"};
    }
    if(body.Nombre == undefined){
        return {Error: true, Msg: "Campo 'Nombre' no encontrado"};
    }
    if(body.Descripcion == undefined){
        return {Error: true, Msg: "Campo 'Descripcion' no encontrado"};
    }
    if(body.Precio == undefined){
        return {Error: true, Msg: "Campo 'Precio' no encontrado"};
    }
    if(body.Visible == undefined){
        return {Error: true, Msg: "Campo 'Visible' no encontrado"};
    }
    var categoria = await mDataCategoria.getCategoriaById(body.IdCategoria);
    if(categoria == null || categoria == undefined){
        return {Error: true, Msg: "La categoria seleccionada no es válida"};
    }
    if(body.Codigo.trim() == ""){
        return {Error: true, Msg: "El código ingresado no es válido"};
    }
    prod = await mData.getProductosByCodigoEmpresa(body.Codigo.trim(), body.IdEmpresa);
    if((prod != null && prod != undefined) && prod._id != idProducto){
        return {Error: true, Msg: "Ya existe un producto con este código"};
    }
    if(body.Nombre.trim() == ""){
        return {Error: true, Msg: "El nombre ingresado no es válido"};
    }
    if(body.Descripcion.trim() == ""){
        return {Error: true, Msg: "La descripción ingresada no es válida"};
    }
    try{
        body.Precio = parseFloat(body.Precio);
        if(body.Precio < 0 || isNaN(body.Precio)){
            return {Error: true, Msg: "El precio ingresado no es válido"};
        }
    }catch(err){
        return {Error: true, Msg: "El precio ingresado no es válido"};
    }
    if(body.Visible.trim() != "SI" && body.Visible.trim() != "NO"){
        return {Error: true, Msg: "El dato del campo visible no es válido"};
    }
    return {Error: false, Msg: "Actualizado correctamente"};
};

exports.validarPutFoto = async function (idProducto,body){
    var prod = await mData.getProductosById(idProducto);
    if(prod == null || prod == undefined){
        return {Error: true, Msg: "El producto no es válido"};
    }
    if(body.Archivo == undefined ||body.Archivo == null){
        return {Error: true, Msg: "Archivo no válido"};
    }
    if(body.Archivo.Data == ""){
        return {Error: true, Msg: "Archivo no válido"};
    }
    return {Error: false, Msg: "Actualizado correctamente"};
};
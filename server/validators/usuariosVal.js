'use strict'

const UsuariosData = require("../data/usuariosData");

const utils = require("../utils/utils");

exports.validarLogin = function (data){
	if(data.Email == undefined){
        return {Error: true, Msg: "Campo 'Email' no encontrado"};
    }
    if(data.Pass == undefined){
        return {Error: true, Msg: "Campo 'Pass' no encontrado"};
    }
    if(data.Email.trim() == ""){
        return {Error: true, Msg: "Debe digitar un email válido"};
    }
    if(data.Pass.trim() == ""){
        return {Error: true, Msg: "Debe digitar una contraseña válida"};
    }
    if(!utils.validarEmail(data.Email.trim())){
        return {Error: true, Msg: "El email ingresado no es válido"};
    }
    return {Error: false, Msg: "OK"};
};

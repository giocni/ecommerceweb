const mysql = require('mysql');
const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database : 'test'
});

db.connect(function(err) {
  if(err){
    console.log("error al contetar a mysql: " + err);
  }else{
    console.log("Conectado mysql!");
  }
});

module.exports = db;
'use strict'

const  UsuariosData = require('../../data/usuariosData');

var validacion = require('../../validators/usuariosVal');
var md_auth = require('../../middlewares/authenticated');

router.post('/usuario/login',async function(req,res){
    var result = await validacion.validarLogin(req.body);
    if(result.Error){
        res.json(result);
        return;
    }
    result = await UsuariosData.login(req.body);
    res.json(result);
});



'use strict'

const  mData = require('../../data/productosData');

var validacion = require('../../validators/productosVal');
var md_auth = require('../../middlewares/authenticated');

router.get('/public/productos/:idCategoria',async function(req,res){
    const idCategoria = req.params.idCategoria;
    const result = await mData.getProductosByCategoria(idCategoria);
    res.json(result);
});

router.get('/productos/empresa',md_auth.ensureAuth,async function(req,res){
    const user = req.user;
    const result = await mData.getProductosByEmpresaAll(user.IdEmpresa);
    res.json(result);
});

router.post('/producto',md_auth.ensureAuth,async function(req,res){
    const user = req.user;
    req.body.IdEmpresa = user.IdEmpresa;
    var result = await validacion.validarPost(req.body);
    if(result.Error){
        res.json(result);
        return;
    }
    const idProducto = await mData.post(req.body);
    res.json(result);
});

router.put('/producto/:idProducto',md_auth.ensureAuth,async function(req,res){
    const user = req.user;
    const idProducto = req.params.idProducto;
    var result = await validacion.validarPut(idProducto,req.body);
    if(result.Error){
        res.json(result);
        return;
    }
    await mData.put(idProducto,req.body);
    res.json(result);
});

router.put('/producto/foto/:idProducto',md_auth.ensureAuth,async function(req,res){
    const user = req.user;
    const idProducto = req.params.idProducto;
    var result = await validacion.validarPutFoto(idProducto,req.body);
    if(result.Error){
        res.json(result);
        return;
    }
    await mData.putFoto(idProducto,req.body);
    res.json(result);
});
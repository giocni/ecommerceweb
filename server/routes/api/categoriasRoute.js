'use strict'

const  mData = require('../../data/categoriasData');

//var validacion = require('../../validators/productosVal');
var md_auth = require('../../middlewares/authenticated');

router.get('/public/categorias',async function(req,res){
    const result = await mData.getCategoriasAll();
    res.json(result);
});
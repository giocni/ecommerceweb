routerViews.get('/',(req,res) => {
    res.sendFile('public/index.html', { root: './' });
});

routerViews.get('/admin',(req,res) => {
    res.sendFile('public/indexAdmin.html', { root: './' });
});

routerViews.get('/foto/producto/:nombre', function(req,res){
    var nombre = req.params.nombre;
    res.sendFile('assets/images/products/' + nombre, {root: './'});
    return;
});
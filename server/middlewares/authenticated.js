const jwt = require('jwt-simple');
const moment = require('moment');

const secret = '$_Kala_1423_$';

exports.ensureAuth = function(req, res, next){
    if(!req.headers.authorization){
        return res.status(403).send({message: 'La peticion no tiene la cabecera de autenticación'});
    } else {
        var token = req.headers.authorization.split(" ")[1];
        try{
            var payload = jwt.decode(token, secret);
            if(payload.exp > moment().unix()){
                return res.status(401).send({
                    message: 'EL token ha expirado'
                });
            }
        } catch (ex){
            console.log(ex);
            return res.status(404).send({
                message: 'EL token no es valido'
            });
        }
        req.user = payload;
        next();
    }
}

exports.ensureAuthViews = function(req, res, next){
    if(!req.headers.authorization){
        return res.status(200).sendFile('public/views/noSesion.html', { root: './' });
    } else {
        var token = req.headers.authorization.split(" ")[1];
        try{
            var payload = jwt.decode(token, secret);
            if(payload.exp > moment().unix()){
                return res.status(200).sendFile('public/views/tokenExpirado.html', { root: './' });
            }
        } catch (ex){
            console.log(ex);
            return res.status(200).sendFile('public/views/tokenNoValido.html', { root: './' });
        }
        req.user = payload;
        next();
    }
}

exports.generateToken = function(usuario){
    return jwt.encode(usuario, secret);
}
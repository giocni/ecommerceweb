// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue    from 'vue'
import App    from './App'
import router from './router'
import store  from '../config/store'
import Axios  from 'axios'
import VueAWN from "vue-awesome-notifications"

require("vue-awesome-notifications/dist/styles/style.css")

let options = {}
Vue.use(VueAWN, options)

Vue.prototype.$http = Axios;
const token = localStorage.getItem('token');
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = "Bearer " + token;
}

Vue.config.productionTip = false;

new Vue({
	el: '#app',
	store,
	router,
	VueAWN,
  	components: { App },
  	template: '<App/>'
});
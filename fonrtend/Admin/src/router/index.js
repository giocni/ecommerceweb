import Vue                  from 'vue'
import Router               from 'vue-router'
import store                from '../../config/store'
import Inicio               from '@/components/inicio/Inicio'
import LoginCliente         from "@/components/clientes/loginCliente"
import LoginUsuario         from "@/components/usuarios/LoginUsuario"
import RegistrarProducto    from "@/components/productos/RegistrarProducto"

Vue.use(Router)

const router = new Router({
	routes: [
    {
    	path: '/',
    	name: 'Login',
		component: LoginCliente
	},
	{
		path: '/registro',
		name: 'Registro',
		component: LoginUsuario
	},
	{
    	path: '/inicio',
    	name: 'Inicio',
		component: Inicio,
		meta: {
			requiresAuth: true
		},
		children: [
			{
				path: 'productos',
				name: 'Gestión de productos',
				component: RegistrarProducto,
				meta: {
					requiresAuth: true
				}
			}
		]
	}
  ]
});

/*para validar si el usuario está logueado
router.beforeEach((to, from, next) => {
	if (to.matched.some(record => record.meta.requiresAuth)) {
	  if (store.getters.isLoggedIn) {
		next()
		return
	  }
	  next('/login')
	} else {
	  next()
	}
  });*/

export default router;